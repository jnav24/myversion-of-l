var jnPaper = {
	init:function(pt) {
		if(pt == '' || pt == null) {
			pt = '.row'
		}
		
		if(pt.indexOf('.') != 0) {
			pt = '.' + pt;
		}

		this.eLabel(pt);
	},

	eLabel:function(pt) {
		var $this = this; // points to the object

		$(pt).on('click', 'label', function() {
			$this.setAnimate(this);
		}).on('blur', 'input', function() {
			$this.validateFields(this)
		}).on('focus', 'input', function() {
			$this.setAnimate(this)
		});
	},

	setAnimate:function(dis) {
		var tDis = $(dis),
			nDis = tDis.parent();

		if( tDis.is('input') == false ) {
			nDis.find('input').focus();
		}

		this.runAnimate(nDis, tDis);
	},

	runAnimate:function(dis, dat) {
		dis.find('label').addClass(this.myClass());
		dat.addClass(this.myClass());
	},

	validateFields:function(dis) {
		var dis = $(dis);
		if( dis.val() == '' ) {
			dis.parent().find('label').removeClass(this.myClass());
			dis.removeClass();
		}
	},

	myClass:function() {
		return 'animate';
	},
};