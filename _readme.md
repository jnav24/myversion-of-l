My version of L

Inspired by Google Material Design, from Google i/o 2014, I decided to give some of the form elements, you can find from their site, a try. I know there is an angular version and so forth but I learn by creating.

You must have a parent container that contains a label and a input field. by default, the js will expect the parent container to be named 'row' but you can call it whatever you want. Just in the html, call the object jnPaper.init() and if you want to rename the parent to, for example myParent, then jnPaper.init('myParent')